﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{

    public class Bot : MonoBehaviour
    {
        private int Level;
        private float Speed;
        private bool isBotFrozen;

        private void Start()
        {
            Level = CalculateLevel(10, 15);
            isBotFrozen = CalculateifFrozen(true, true);
        }

        public void SetLevel()
        {

        }

        public void SetLevel(int Level)
        {

        }

        public void CalculateLevel(int a, int b, int c)
        {

        }

        public void SetLevel(int Level, float Speed, bool isBotFrozen)
        {

        }

        public int CalculateLevel(int a, int b)
        {
            int result = a + b;
            return result;
        }

        public bool CalculateifFrozen(bool a, bool b)
        {
            bool result = a & b;
            return result;
        }
    }
}
