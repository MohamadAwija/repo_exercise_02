﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{

    public class Enemy : MonoBehaviour
    {
        public int enemyHealth = 150;
        public int enemyLevel = 15;
        public float attackDamage = 25.5f;
        public string enemyItem = "Axe";
        public bool isEnemyAlive = true;

        private void Start()
        {
            SetenemyHealth(200, 20, 27.6f, "Mace", true);
        }

        public void SetenemyHealth(int newenemyHealth, int newenemyLevel, float newattackDamage, string newenemyItem, bool newisEnemyAlive)
        {
            enemyHealth = newenemyHealth;
            enemyLevel = newenemyLevel;
            attackDamage = newattackDamage;
            enemyItem = newenemyItem;
            isEnemyAlive = newisEnemyAlive;
        }
    }
}